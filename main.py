# Este código es una aplicación CLI que utiliza la API de OpenAI para generar código en Bash a partir de una descripción de la tarea proporcionada por el usuario.

# Importar librerías

import os
import requests # pip install requests
import sys

# Cargar variables de entorno

import dotenv # pip install python-dotenv

# Cargar configuración de variables de entorno
dotenv.load_dotenv()
token = os.getenv("OPENAI_API_KEY")

# Cargar los argumentos de la línea de comandos, en forma de prompt 
def argsToText(): # nos devuelve un string con los argumentos
    return ' '.join(sys.argv[1:])

prompt = argsToText()

# Hacer una request a la API de OpenAI
headers = {
    "Authorization": f"Bearer {token}"
}

# Funcion build_prompt
def build_prompt(prompt):
    return f"""
        # Bash script para windows powershell con el siguiente proposito: {prompt}
        """

payload = {
    "top_p": 1,
    "stop": "```",
    "temperature": 0,
    "suffix": "\n```",
    "max_tokens": 1000,
    "presence_penalty": 0,
    "frequency_penalty": 0,
    "model": "text-davinci-003",
    "prompt": build_prompt(" ".join(prompt)),
}

response = requests.post("https://api.openai.com/v1/completions", headers=headers, json=payload)

if response.status_code >= 400 and response.status_code < 500:
    response_body = response.json()
    error_message = response_body["error"]["message"]
    print(f"API error: \"{error_message}\"")
    sys.exit(1)
elif response.status_code >= 500:
    print(f"OpenAI is currently experiencing problems. Status code: {response.status_code}")
    sys.exit(1)

code = response.json()["choices"][0]["text"].strip()

print(code)